package com.example.demoavrojava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAvroJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAvroJavaApplication.class, args);
	}

}
