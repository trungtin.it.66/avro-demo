package com.example.demoavrojava.avro;

import org.apache.avro.Schema;
import org.apache.avro.compiler.specific.SpecificCompiler;

import java.io.File;
import java.io.IOException;

public class AvroClassGenerator {
    public void generateAvroClasses(String payload ) {
        try {
            SpecificCompiler compiler = new SpecificCompiler(new Schema.Parser().parse(payload));
            compiler.compileToDestination(new File("src/main/resources"), new File("src/main/java"));
        } catch ( IOException ignored){

        }
    }
}
