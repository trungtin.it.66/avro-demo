package com.example.demoavrojava.avro;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class Controller {
    @PostMapping("/gen")
    public void genarate(@RequestBody String payload ){
        AvroClassGenerator avroClassGenerator = new AvroClassGenerator();
        avroClassGenerator.generateAvroClasses(payload);
    }

    @GetMapping("/gen")
    public void genarate() throws IOException {
        AvroSerialize.run();
    }
}
