package com.example.demoavrojava.avro;

import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class AvroSerialize {
    public static void run() throws IOException {

        //Instantiating generated emp class
        Charge charge1=new Charge();

        charge1.setCardNo("4242 4242 4242 4242");
        charge1.setExpiredDate("10/30");
        charge1.setAmount(100);
        charge1.setBrand("visa");
        charge1.setCvv("007");
        charge1.setId(UUID.randomUUID().toString());
        charge1.setStatus("success");

        Charge charge2=new Charge();

        charge2.setCardNo("4242 4242 4242 4242");
        charge2.setExpiredDate("10/30");
        charge2.setAmount(200);
        charge2.setBrand("visa");
        charge2.setCvv("007");
        charge2.setId(UUID.randomUUID().toString());
        charge2.setStatus("success");

        //Instantiate DatumWriter class
        DatumWriter<Charge> empDatumWriter = new SpecificDatumWriter<>(Charge.class);
        DataFileWriter<Charge> empFileWriter = new DataFileWriter<Charge>(empDatumWriter);

        empFileWriter.create(charge1.getSchema(), new File("src/main/resources/charge_gen.avro"));

        empFileWriter.append(charge1);
        empFileWriter.append(charge2);

        empFileWriter.close();

        System.out.println("data successfully serialized");
    }
}
