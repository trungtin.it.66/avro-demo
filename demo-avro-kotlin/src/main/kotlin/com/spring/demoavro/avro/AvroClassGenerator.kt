package com.spring.demoavro.avro

import org.apache.avro.Schema
import org.apache.avro.compiler.specific.SpecificCompiler
import org.springframework.stereotype.Component
import java.io.File

import java.io.IOException

@Component
class AvroClassGenerator {
    fun generateAvroClasses(payload: String) {
        try {
            val compiler = SpecificCompiler(Schema.Parser().parse(payload))
            compiler.compileToDestination(File("src/main/resources"), File("src/main/java"))
        } catch (ex: IOException){

        }
    }

}