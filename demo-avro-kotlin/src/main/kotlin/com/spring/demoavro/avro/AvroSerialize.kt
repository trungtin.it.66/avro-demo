package com.spring.demoavro.avro

import org.apache.avro.file.DataFileWriter
import org.apache.avro.io.DatumWriter
import org.apache.avro.specific.SpecificDatumWriter
import org.springframework.stereotype.Component
import java.io.File
import java.util.*

@Component
class AvroSerialize {

    fun run() {
        try {
            //Instantiating generated emp class
            val charge1 = Charge()
            charge1.setCardNo("4242 4242 4242 4242")
            charge1.setExpiredDate("10/30")
            charge1.setAmount(100)
            charge1.setBrand("visa")
            charge1.setCvv("007")
            charge1.setId(UUID.randomUUID().toString())
            charge1.setStatus("success")
            val charge2 = Charge()
            charge2.setCardNo("4242 4242 4242 4242")
            charge2.setExpiredDate("10/30")
            charge2.setAmount(200)
            charge2.setBrand("visa")
            charge2.setCvv("007")
            charge2.setId(UUID.randomUUID().toString())
            charge2.setStatus("success")

            //Instantiate DatumWriter class
            val empDatumWriter: DatumWriter<Charge> = SpecificDatumWriter(Charge::class.java)
            val empFileWriter = DataFileWriter(empDatumWriter)
            empFileWriter.create(charge1.schema, File("src/main/resources/charge_gen.avro"))
            empFileWriter.append(charge1)
            empFileWriter.append(charge2)
            empFileWriter.close()
            println("data successfully serialized")
        } catch ( ex : Exception){

        }
    }
}