package com.spring.demoavro.avro

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class Controller(
    var avroClassGenerator: AvroClassGenerator,
    var avroSerialize: AvroSerialize
){
    @PostMapping("/gen")
    fun genarateBySchema(@RequestBody payload: String): String{
        avroClassGenerator.generateAvroClasses(payload)
        return "Successfully"
    }

    @GetMapping("/gen")
    fun genarateArvoFile() : String{
        avroSerialize.run()
        return "Successfully"
    }
}