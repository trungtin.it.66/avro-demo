package com.spring.demoavro

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoAvroApplication

fun main(args: Array<String>) {
	runApplication<DemoAvroApplication>(*args)
}
